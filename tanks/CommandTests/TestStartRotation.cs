using System;
using Xunit;
using Moq;
using Core;
using Core.Rotation;

namespace Tests
{
    public class TestStartRotation
    {
        [Fact]
        public void TestStartRotationCommand()
        {
            //Arrange
            var maxDirections = 8;
            var angularVelocity = 6;

            var rotationStartableObj = new Mock<IRotationStartable>();

            rotationStartableObj.Setup(obj => obj.SetMaxNumberOfDirections(maxDirections)).Verifiable();
            rotationStartableObj.Setup(obj => obj.SetAngularVelocity(angularVelocity)).Verifiable();
            rotationStartableObj.Setup(obj => obj.SetRotationInjector(It.IsAny<ICommand>())).Verifiable();
            rotationStartableObj.Setup<UObject>(obj => obj.GetSource()).Returns(It.IsAny<UObject>()).Verifiable();

            StartRotation startRotationCmd = new StartRotation(rotationStartableObj.Object, angularVelocity, maxDirections);

            //Act
            startRotationCmd.Execute();

            //Assert
            rotationStartableObj.VerifyAll();
        }
    }
}
