﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Core.Rotation;
using Core.Injector;
using Moq;
using Core;

namespace Tests
{
    public class TestStopRotate
    {
        [Fact]
        public void Test1()
        {
            //Arrange

            var rotationStoppableObj = new Mock<IRotateStoppable>();
            var inj = new Mock<IInjector>();


            rotationStoppableObj.Setup(obj => obj.GetRotationCommand()).Returns(inj.Object).Verifiable();
            rotationStoppableObj.Setup(obj => obj.RemoveAngularVelocity()).Verifiable();
            rotationStoppableObj.Setup(obj => obj.RemoveRotateCommand()).Verifiable();
            rotationStoppableObj.Setup(obj => obj.RemoveMaxNumbersOfDirections()).Verifiable();
            inj.Setup(obj => obj.Inject(It.IsAny<EmptyCmd>())).Verifiable();

            StopRotate stopRotate = new StopRotate(rotationStoppableObj.Object);

            //Act
            stopRotate.Execute();

            //Assert
            rotationStoppableObj.VerifyAll(); 
        }
    }
}
