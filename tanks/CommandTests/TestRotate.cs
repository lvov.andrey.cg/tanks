﻿using System;
using Core.Rotation;
using Moq;
using Xunit;

namespace Tests
{
    
    public class TestRotate
    {
        [Fact]
        public void TestRotateInDiscreteMomentInTimeAngle45() //Тест поворота. круг поделен на 8 частей, начальная позиция на 3 доле, скорость - 6 долей в дискретный момент времени. Танк делает поворот и оказывается на 9(1) доле, что равно 45 градусам.
        {
            //Arrange
            var rotatableObj = new Mock<IRotatable>();

            rotatableObj.Setup(obj => obj.GetMaxNumberOfDirections()).Returns(8).Verifiable();
            rotatableObj.Setup(obj => obj.GetDirection()).Returns(3).Verifiable();
            rotatableObj.Setup(obj => obj.GetAngularVelocity()).Returns(6).Verifiable();
            rotatableObj.Setup(obj => obj.ChangeDirection(It.Is<int>(x => x == (3 + 6)))).Returns(It.Is<int>(x => x == 9)).Verifiable();
            rotatableObj.Setup(obj => obj.SetAngle(It.Is<double>(x => x == 9 % 8 / 8 * 2 * Math.Round(Math.PI, 3)))).Verifiable();

            Rotate rotateCmd = new Rotate(rotatableObj.Object);
            
            //Act
            rotateCmd.Execute();

            //Assert
            rotatableObj.VerifyAll();
        }

        [Fact]
        public void TestRotateInDiscreteMomentInTimeAngleii45() //Отрицательный угол - поворот по часовой стрелке. угол = -45.
        {
            //Arrange
            var rotatableObj = new Mock<IRotatable>();

            rotatableObj.Setup(obj => obj.GetMaxNumberOfDirections()).Returns(8).Verifiable();
            rotatableObj.Setup(obj => obj.GetDirection()).Returns(1).Verifiable();
            rotatableObj.Setup(obj => obj.GetAngularVelocity()).Returns(-2).Verifiable();
            rotatableObj.Setup(obj => obj.ChangeDirection(It.Is<int>(x => x == (1 - 2)))).Returns(It.Is<int>(x => x == -1)).Verifiable();
            rotatableObj.Setup(obj => obj.SetAngle(It.Is<double>(x => x == -1 % 8 / 8 * 2 * Math.Round(Math.PI, 3)))).Verifiable();

            Rotate rotateCmd = new Rotate(rotatableObj.Object);

            //Act
            rotateCmd.Execute();

            //Assert
            rotatableObj.VerifyAll();
        }

        [Fact]
        public void TestRotateInDiscreteMomentInTimeAngle272()
        {
            //Arrange
            var rotatableObj = new Mock<IRotatable>();

            rotatableObj.Setup(obj => obj.GetMaxNumberOfDirections()).Returns(360).Verifiable();
            rotatableObj.Setup(obj => obj.GetDirection()).Returns(15).Verifiable();
            rotatableObj.Setup(obj => obj.GetAngularVelocity()).Returns(257).Verifiable();
            rotatableObj.Setup(obj => obj.ChangeDirection(It.Is<int>(x => x == (15 + 257)))).Returns(It.Is<int>(x => x == 272)).Verifiable();
            rotatableObj.Setup(obj => obj.SetAngle(It.Is<double>(x => x == 272 % 360 / 360 * 2 * Math.Round(Math.PI, 3)))).Verifiable();

            Rotate rotateCmd = new Rotate(rotatableObj.Object);

            //Act
            rotateCmd.Execute();

            //Assert
            rotatableObj.VerifyAll();
        }
    }
}
