﻿namespace Core
{
    public interface UObject
    {
        object this[string property] { get; }

        void setProperty(string property, object value);
        bool hasProperty(string property);
        void removeProperty(string property);

    }
}