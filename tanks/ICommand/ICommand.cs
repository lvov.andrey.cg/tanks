﻿using System;

namespace Core
{
    public interface ICommand
    {
        public void Execute();
    }
}
