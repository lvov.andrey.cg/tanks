﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core
{
    public class Vector
    {
        private List<int> vector;

        public Vector(int[] values)
        {
            vector = values.ToList();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            else
            {
                return this.vector.SequenceEqual(((Vector)obj).vector);
            }
        }

        public override int GetHashCode()
        {
            return this.vector.GetHashCode();
        }
    }
}
