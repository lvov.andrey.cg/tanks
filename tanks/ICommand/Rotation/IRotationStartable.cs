﻿using System;

namespace Core.Rotation
{
    public interface IRotationStartable
    {
        UObject GetSource();
        void SetAngularVelocity(int angularVelocity);
        void SetMaxNumberOfDirections(int maxNumberOfDirections);
        void SetRotationInjector(ICommand injector);
    }
}
