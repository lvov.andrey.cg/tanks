﻿namespace Core.Injector
{
    public interface IInjector
    {
        void Inject(ICommand cmd);
    }
}
