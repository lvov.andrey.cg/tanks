﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Rotation
{
    public interface IRotatable
    {
        int GetDirection(); //направление танка в дискретный момент времени

        int GetAngularVelocity(); //значение, на которое изменится направление танка в дискретный момент времени

        int GetMaxNumberOfDirections(); //максимальное количество направлений

        int ChangeDirection(int angularVelocity); //повернуть танк в дискретный момент времени

        void SetAngle(double changedDirection);
    }
}
