﻿using System;

namespace Core.Rotation
{
    public class StartRotation : ICommand
    {
        private IRotationStartable rotationStartableObj;
        private int angularVelocity;
        private int maxNumberOfDirections;

        public StartRotation(IRotationStartable obj, int angularVelocity, int maxNumberOfDirections)
        {
            this.rotationStartableObj = obj;
            this.angularVelocity = angularVelocity;
            this.maxNumberOfDirections = maxNumberOfDirections;
        }

        public void Execute()   
        {
            rotationStartableObj.SetMaxNumberOfDirections(this.maxNumberOfDirections); //Задали максимальное число направлений
            rotationStartableObj.SetAngularVelocity(this.angularVelocity); //Задали угловую скорость 

            ICommand rotateCmd = IoC.Resolve<ICommand>("GetRotationCommand", rotationStartableObj.GetSource());
            ICommand injector = IoC.Resolve<ICommand>("GetInjector", rotateCmd);
            rotationStartableObj.SetRotationInjector(injector);
            ICommand pushCmd = IoC.Resolve<ICommand>("GetPushCommand", injector);

            pushCmd.Execute();
        }
    }
} 