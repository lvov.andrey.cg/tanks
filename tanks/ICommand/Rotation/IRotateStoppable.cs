﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Injector;

namespace Core.Rotation
{
    public interface IRotateStoppable
    {
        IInjector GetRotationCommand();
        void RemoveAngularVelocity();
        void RemoveRotateCommand();
        void RemoveMaxNumbersOfDirections();
    }
}
