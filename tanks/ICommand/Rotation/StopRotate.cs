﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Injector;

namespace Core.Rotation
{
    public class StopRotate : ICommand
    {
        private IRotateStoppable rotationStoppableObj;

        public StopRotate(IRotateStoppable obj)
        {
            this.rotationStoppableObj = obj;
        }

        public void Execute()
        {
            IInjector rotateCmd = rotationStoppableObj.GetRotationCommand();
            ICommand emptyCmd = IoC.Resolve<ICommand>("EmptyCmd");

            rotateCmd.Inject(emptyCmd);

            rotationStoppableObj.RemoveAngularVelocity();
            rotationStoppableObj.RemoveRotateCommand();
            rotationStoppableObj.RemoveMaxNumbersOfDirections();
        }
    }
}
