﻿using Core.Injector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Queue
    {
        public List<IInjector> queue;
        public Queue(IInjector queue)
        {
            this.queue.Add(queue);
        }
    }
}
