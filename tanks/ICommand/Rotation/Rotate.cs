﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Rotation
{
    public class Rotate : ICommand
    {
        IRotatable obj;
        
        public Rotate(IRotatable obj)
        {
            this.obj = obj;
        }

        public void Execute()
        {
            obj.SetAngle((obj.ChangeDirection(obj.GetDirection() + obj.GetAngularVelocity()) % obj.GetMaxNumberOfDirections()) / obj.GetMaxNumberOfDirections() * 2 * Math.PI);//новый угол(поворот)

            ICommand injector = IoC.Resolve<ICommand>("injectorCmd", this.obj);
            ICommand pushCmd = IoC.Resolve<ICommand>("pushCmd", injector);
            pushCmd.Execute();//закинуть опять в очередь
        }
    }

    
}
